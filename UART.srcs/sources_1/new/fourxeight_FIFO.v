`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date: 09/05/2018 01:18:39 PM
// Design Name: 
// Module Name: 4x8_FIFO
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// This is a general purpose nx8 FIFO that uses synchronous
//	latchin and latchout control signals.
//	It will first be used in the UART I am building for the
//	COMPE 470 class but will be built using parameters so that it
//	can be used in other projects
//////////////////////////////////////////////////////////////////////////////////

//if you change these values
//then update the addr size
localparam depth = 4;
localparam width = 8;
localparam addrSize = 2;

module fourxeight_FIFO


	
	(
    input [7:0] FIFO_IN,
    output [7:0] FIFO_OUT,
	input latchin,
	input latchout,
    input clk,
    output isFull,
    output isEmpty
    );
	
	//set these to be equal
	//to log2 of the depth
	//I can't be bothered
	//to figure out the work
	//around to do this
	reg [addrSize-1:0] wrAddr;
	reg [addrSize-1:0] reAdder;
	
	//this is the fifo buffer
	reg [width-1:0] buffer [depth-1:0];
	
	//Figure out the full and empty flags
	//Circular buffer is used so figure out how to deal
	//with detecting whether the write pointer gets within
	//a critical range of the read pointer
	//Care must be taken to make sure that wrap around
	//logic of the circular buffer
	
	//assignment to update the isFull flag
	//doesn't work with edge cases
	//assign isFull = (writeAddr+17 >=readAddr) ? 1:0;
	
	//assignment to update the isEmpty flag
	assign isEmpty = (writeAddr==readAddr) ? 1:0;
	
	//main part of the module
	//set to zero upon reset
	//when writing to the ram make sure that
	//latchIn and latchOut are 1 for only one clock cycle
	//putting in a one shot built in so that they are internal
	//to this module
	//
	always@(posedge clk)
	begin
		//reset memory to 0
		if(reset)
		begin
			readAddr 	<= 0;
			writeAddr 	<= 0;
			out 		<= 8'h00;
			for(iterator = 0 ; iterator <(depth-1); iterator = iterator +1)
			begin
				buffer[iterator] <= 0;
			end
		end
		
		
	end
	
endmodule
